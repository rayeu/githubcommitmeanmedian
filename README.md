# README #

# Quick summary #
Service that runs at configurable intervals and calculates the mean and median length of GitHub messages stored in BigTable.

# Repo Used #
https://bigquery.cloud.google.com/dataset/githubarchive:day


# Requirements #

### SDK ###

* google-cloud-sdk

### Python Libraries ###

* gevent
* google-auth-oauthlib
* google-cloud-bigquery

# Steps to run #

* Download and unzip the source code or clone the repository.
* cd <project-root>
* Edit **./config/google_config.json** file and update the project field to contain the id of google cloud project used for bigquery billing.

```
#!json

{
  "project": "<project-id-to-used-for-billing>",
  "open_browser_for_auth": true
}
```

* Run: **./mean_meadina_service.py**

# Authentication #
The project uses OAuth 2.0 authentication.