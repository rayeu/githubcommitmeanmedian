#!/usr/bin/env python

"""
mean_median_service is a service that queries the BigTable's github archive
at configured interval and computes running mean and median values of the length of
commit messages.
"""

from gevent import monkey;

monkey.patch_all()

from util import conf
from lib import fetcherloop


def _parse_args():
    service_config = conf.parse_service_config()
    google_config = conf.parse_google_config()
    return service_config, google_config


def main():
    service_config, google_config = _parse_args()
    fetcherloop.start(service_config, google_config)


main()
