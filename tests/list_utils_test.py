
import unittest

from util import list_utils


class ListUtilsTest(unittest.TestCase):
    def test_append_at_end(self):
        sorted_list = [1, 2, 3, 4, 5]
        list_utils.append_sorted(sorted_list, 6)
        self.assertListEqual(sorted_list, [1, 2, 3, 4, 5, 6])

    def test_append_at_beginning(self):
        sorted_list = [1, 2, 3, 4, 5]
        list_utils.append_sorted(sorted_list, 0)
        self.assertListEqual(sorted_list, [0, 1, 2, 3, 4, 5])

    def test_append_at_middle(self):
        sorted_list = [1, 2, 3, 4, 5]
        list_utils.append_sorted(sorted_list, 3.5)
        self.assertListEqual(sorted_list, [1, 2, 3, 3.5, 4, 5])

    def test_append_sorted(self):
        sorted_list = [1, 3, 5, 7, 9]
        k = 2
        list_utils.append_sorted(sorted_list, k)
        self.assertItemsEqual(sorted_list, [1, 2, 3, 5, 7, 9])


if __name__ == "__main__":
    unittest.main()
