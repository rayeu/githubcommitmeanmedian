"""
Steps to run this test case:
    1. cd to project root
    2. python tests/math_utils_test.py
"""

import unittest

from util import math_utils


class MathUtilsTest(unittest.TestCase):
    def test_verify_mean(self):
        data = {1: 6, 2: 3, 3: 3}
        self.assertEqual(math_utils.compute_mean(0, 0, data)[0], 1.75)

    def test_verify_median_odd(self):
        data = {"1": 5, "2": 3, "3": 3}
        sorted_keys = [1, 2, 3]
        self.assertEqual(math_utils.compute_median(data, sorted_keys), 2)

    def test_verify_median_even(self):
        data = {"1": 5, "2": 2, "3": 3}
        sorted_keys = [1, 2, 3]
        self.assertEqual(math_utils.compute_median(data, sorted_keys), 1.5)


if __name__ == "__main__":
    unittest.main()
