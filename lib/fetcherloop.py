
import datetime
import json
import logging

import gevent

from collections import defaultdict
from lib.bigquery_executor import BigQueryExecutor
from util import query_utils, math_utils, list_utils

# Global variables needed to keep data in memory throughout
# the life of the program.
executor = None
memo = {}

logging.basicConfig(level=logging.INFO)


def display_result(mean, median, date):
    logging.info("Date: %s, Mean: %s, Median: %s", date, mean, median)


"""
Args:
    param1: a json loaded dict type which has all the details (memo_file,
    history_file, fetch_interval) that the service needs in order to run.

Details:
    Executes queries against the BigTable's Github archive and computes running
    mean and median values of length of commit messages. Runs on a separate greenlet.
"""
def fetch_job(config):
    global memo
    logging.debug("fetch_job; Executing query against BigTable; config=%s", config)

    memo_file = str(config["memo_file"])

    # Load memo if it is not present.
    if not memo:
        try:
            with open(memo_file) as f:
                memo = json.loads(f.read())
        except:
            memo = {"stat": {}, "track": {}, "sorted_keys": []}
            logging.debug("fetch_job; No memo file found, creating one.")

    memo_stat = memo["stat"]
    memo_track = memo["track"]
    sorted_keys = memo["sorted_keys"]

    # If no memo is present, then we start from current time - 6 hours.
    try:
        last_date = query_utils.str_to_date(memo_track["last_date"])
    except:
        last_date = datetime.datetime.utcnow() - datetime.timedelta(hours=6)
        s = query_utils.date_to_str(last_date)
        last_date = query_utils.str_to_date(s)

    try:
        last_id = memo_track["last_id"]
    except:
        last_id = None

    if last_id:
        query = query_utils.get_query_with_start_time_and_id(last_date, last_id)
    else:
        query = query_utils.get_query_with_start_time(last_date)

    logging.debug("fetch_job; query_string='%s'", query)

    # TODO: Execute query to compute max(id) and max(creted_at) instead of computing them
    # manually.
    new_stat = defaultdict(int)
    for row in executor.execute_query(query):
        # row[0]: id, row[1]: created_at, row[2]: commits
        last_id = max(last_id, row[0])
        last_date = max(last_date, row[1].replace(tzinfo=None))
        for commit in json.loads(row[2])["commits"]:
            new_stat[len(commit["message"])] += 1


    try:
        last_mean, last_count = memo_track["last_mean"], memo_track["last_count"]
        last_median = memo_track["last_median"]
    except:
        last_mean, last_count, last_median = 0, 0, 0

    # If there is no new data, there is no point continuing.
    if not new_stat:
        logging.debug("fetch_job; no new data")
        display_result(last_mean, last_median, last_date)
        return

    # Now compute mean median using the up-to-date data.
    mean, count = math_utils.compute_mean(last_mean, last_count, new_stat)

    # Merge the two stats to create a combined one.
    for k, v in new_stat.items():
        try:
            memo_stat[str(k)] += v
        except:
            # Add new keys to sorted_keys list maintaining order.
            list_utils.append_sorted(sorted_keys, k)
            memo_stat[str(k)] = v

    median = math_utils.compute_median(memo_stat, sorted_keys)

    display_result(mean, median, last_date)

    # A persistent database which records all the mean and median values along
    # with the time. It is currently only used to store pre-computed information.
    # TODO: expose some APT which allows querying mean median at earlier date.
    history_file = str(config["history_file"])
    with open(history_file, "ab+") as f:
        f.write("date=%s;mean=%s;median=%s\n" % (last_date, mean, median))

    memo_track["last_mean"] = mean
    memo_track["last_count"] = count
    memo_track["last_median"] = median
    memo_track["last_id"] = last_id
    memo_track["last_date"] = query_utils.date_to_str(last_date)

    with open(memo_file, "w") as f:
        f.write(json.dumps(memo))

    logging.debug("fetch_job; fetching complete, memo dumped")


def _run(func, args, seconds):
    while True:
        func(args)
        gevent.sleep(seconds)


def schedule(func, args, seconds):
    return gevent.spawn(_run, func, args, seconds)


def start(service_config, google_config):
    global executor
    executor = BigQueryExecutor(google_config["project"],
                                "client_secrets.json",
                                google_config["open_browser_for_auth"])
    interval = service_config["interval"]
    schedule(fetch_job, args=service_config, seconds=interval)
    while True:
        gevent.sleep(0.5)
