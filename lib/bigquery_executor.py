
from datetime import datetime

from google.cloud import bigquery
from google_auth_oauthlib import flow

from util import query_utils

"""
Details:
    A simple wrapper around python's google.cloud.bigquery library.
"""
class BigQueryExecutor(object):
    def __init__(self, project, secrets_file, open_browser_for_auth):
        # Create a client.
        self.project = project
        self.secrets_file = secrets_file
        self.open_browser_for_auth = open_browser_for_auth

        self.authenticate()
        self.create_client()


    def authenticate(self):
        appflow = flow.InstalledAppFlow.from_client_secrets_file(
                self.secrets_file,
                scopes=['https://www.googleapis.com/auth/bigquery'])
        if self.open_browser_for_auth:
            appflow.run_local_server()
        else:
            appflow.run_console()
        self.credentials = appflow.credentials

    def create_client(self):
        self.client = bigquery.Client(project=self.project, credentials=self.credentials)

    """
    Args:
        param1: string query
        param2: whether or not to allow use of standard sql syntax
        param3: drain the query results by requesting a page at a time
        param4: number of results in a page

    Returns:
        A generator which can be iterated to get the query results.
    """
    def execute_query(self, query, use_legacy_sql=False, page_token=None, max_results=100000):
        # Start query.
        query_results = self.client.run_sync_query(query)
        query_results.use_legacy_sql = use_legacy_sql
        query_results.run()
        # End query.

        # Start fetch data.
        while True:
            rows, total_rows, page_token = query_results.fetch_data(
                    max_results, page_token=page_token
            )
            for row in rows:
                yield row
            if not page_token:
                break
