
"""
Args:
    param1: a sorted list
    param2: a elemented to be inserted in the sorted list in 'param1'
    mantaining sorted order.

Returns:
    Update the sorted_list to contain 'param2' in sorted order.
"""
def append_sorted(sorted_list, k):
    if not sorted_list:
        return sorted_list.insert(0, k)
    lo, hi = 0, len(sorted_list) - 1
    while lo < hi:
        mid = (lo + hi) / 2
        if sorted_list[mid] < k:
            lo = mid + 1
        elif sorted_list[mid] > k:
            hi = mid - 1
        else:
            return

    if sorted_list[lo] > k:
        sorted_list.insert(lo, k)
    else:
        sorted_list.insert(lo+1, k)
