
import json

SERVICE_CONFIG_PATH = "./config/config.json"
GOOGLE_CONFIG_PATH = "./config/google_config.json"


"""
Returns:
    config as dict() loaded from CONFIG_PATH
"""


def parse_config(path):
    with open(path) as f:
        config = json.loads(f.read())
        return config


def parse_service_config():
    return parse_config(SERVICE_CONFIG_PATH)


def parse_google_config():
    return parse_config(GOOGLE_CONFIG_PATH)