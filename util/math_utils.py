
"""
Args:
    param1: mean to be combined with another dataset's mean
    param2: count of data over which 'param1' mean was computed
    param3: new dict with commit length as keys and their
    corresponding frequencies as values

Returns:
    The combined mean.
"""
def compute_mean(last_mean, last_count, new_stat):
    sum, new_count = 0, 0
    for k, v in new_stat.items():
        sum += v * k
        new_count += v
    new_mean = sum * 1.0 / new_count
    if last_count == 0:
        return new_mean, new_count
    total_count = last_count + new_count
    new_mean = (last_mean * 1.0 * last_count + new_mean * new_count) / total_count
    return new_mean, total_count


"""
Args:
    param1: dict with data values as keys (string) and their
    corresponding frequencies as values.
    param2: keys of dict in 'param1' in sorted order.

Returns:
    The median of elements represened by dict in 'param1'
"""
def compute_median(memo_stat, sorted_keys):
    count = 0
    for k in sorted_keys:
        count += memo_stat[str(k)]
    count_to_middle = 0

    a, b = -1, -1
    for i, k in enumerate(sorted_keys):
        count_to_middle += memo_stat[str(k)]
        if count_to_middle == count / 2:
            a = k
        if count_to_middle > count / 2:
            b = k
            break
    if count % 2 == 0 and a != -1:
        return (a + b) / 2.0

    return b
