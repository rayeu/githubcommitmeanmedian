
"""
Collection of functions that allow easy generation of our queries.
"""

import datetime

query_template_time = """select id, created_at, payload FROM `githubarchive.day.%s`
    where type="PushEvent" and created_at >= "%s";"""


query_template_time_id = """select id, created_at, payload FROM `githubarchive.day.%s`
    where type="PushEvent" and created_at >= "%s"  and id > "%s";"""


query_template_max_id_and_created_at = """select max(id), max(created_at) FROM `githubarchive.day.%s`
    where type="PushEvent" and created_at >= "%s"  and id > "%s";"""

query_template_max_id_and_created_at_without_id = """select max(id), max(created_at) FROM `githubarchive.day.%s`
    where type="PushEvent" and created_at >= "%s";"""

format = '%Y-%m-%d %H:%M:%S UTC'

"""
Args:
    param1: datetime object

Returns:
    param1 object converted to string using 'format'
"""
def date_to_str(date):
    return date.strftime(format)


"""
Args:
    param1: datetime string

Returns:
    param1 converted to datetime using 'format'
"""
def str_to_date(date_str):
    return datetime.datetime.strptime(date_str, format)


"""
Args:
    param1: datetime object

Returns:
    Fills date, created_at of 'query_format_time' with the data extracted
    from 'param1' and returns it.
"""
def get_query_with_start_time(start_time):
    date = "".join(str(start_time.date()).split(" ")[0].split("-"))
    return query_template_time % (date, date_to_str(start_time))


"""
Args:
    param1: datetime object
    param2: string id

Returns:
    Fills date, created_at and id of 'query_format_time_id' with the data
    extracted from 'param1' and 'param2' and returns it.
"""
def get_query_with_start_time_and_id(start_time, _id):
    date = "".join(str(start_time.date()).split(" ")[0].split("-"))
    return query_template_time_id % (date, date_to_str(start_time), _id)


def get_max_id_created_at_query_with_start_time(start_time):
    date = "".join(str(start_time.date()).split(" ")[0].split("-"))
    return query_template_max_id_and_created_at_without_id % (date, date_to_str(start_time))
"""
Args:
    param1: datetime object
    param2: string id

Returns:
    A query string to compute max(id) and max(created_at) values such that:
    max(id) > _id and max(created_at) >= start_time.
"""
def get_max_id_created_at_query_with_start_time_and_id(start_time, _id):
    date = "".join(str(start_time.date()).split(" ")[0].split("-"))
    return query_template_max_id_and_created_at % (date, date_to_str(start_time), _id)